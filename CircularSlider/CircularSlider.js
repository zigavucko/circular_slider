/**
 * Class for drawing responsive circular sliders on the SVG container.
 * @param options   an object containing circular slider properties
 */
function CircularSlider(options) {
    /**
     * Class variables.
     * Read variable values from options.
     */
    const CONTAINER = options['container'];
    const COLOR = options['color'];
    const MIN = options['min'];
    const MAX = options['max'];
    const STEP = options['step'];
    var RADIUS = options['radius'];

    /**
     * Area and scale calculations.
     * Determine width and height available for drawing.
     * Specify slider area drawing dimensions and data drawing dimensions.
     * If area dedicated for data drawing would be too small, then draw dimensions
     * as well as some slider options are rescaled.
     */
    var containerWidth = CONTAINER.clientWidth || CONTAINER.parentNode.clientWidth;
    var containerHeight = CONTAINER.clientHeight || CONTAINER.parentNode.clientHeight;
    var windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    var windowHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    var width = containerWidth < windowWidth ? containerWidth : windowWidth;
    var height = containerHeight < windowHeight ? containerHeight : windowHeight;

    var DRAW_WIDTH, DRAW_HEIGHT, DATA_WIDTH, DATA_HEIGHT;
    var SCALE = 1.0;
    var PORTRAIT;
    if (width >= height) {
        PORTRAIT = false;
        if (height / width <= 0.8) {
            DRAW_WIDTH = height;
            DRAW_HEIGHT = height;
            DATA_WIDTH = width - DRAW_WIDTH;
            DATA_HEIGHT = height;
        } else {
            DRAW_WIDTH = height * 0.8;
            DRAW_HEIGHT = height * 0.8;
            DATA_WIDTH = width - DRAW_WIDTH;
            DATA_HEIGHT = DRAW_HEIGHT;
            RADIUS *= 0.8;
            SCALE = 0.8;
        }
    } else {
        PORTRAIT = true;
        if (width / height <= 0.8) {
            DRAW_WIDTH = width;
            DRAW_HEIGHT = width;
            DATA_WIDTH = width;
            DATA_HEIGHT = height - DRAW_HEIGHT;
        } else {
            DRAW_WIDTH = width * 0.8;
            DRAW_HEIGHT = width * 0.8;
            DATA_WIDTH = DRAW_WIDTH;
            DATA_HEIGHT = height - DRAW_HEIGHT;
            RADIUS *= 0.8;
            SCALE = 0.8;
        }
    }

    /**
     * Helper functions.
     */
    function radToDeg(rad) {
        return rad / Math.PI * 180;
    }
    function degToRad(deg) {
        return deg * Math.PI / 180;
    }
    function polarToCartesian(deg) {
        return {
            'x': CX - (RADIUS * Math.cos(degToRad(deg + 90))),
            'y': CY - (RADIUS * Math.sin(degToRad(deg + 90)))
        }
    }

    /**
     * Draw inner, outer and handler circle.
     * Inner circle is underlying circle with gray stroke.
     * Outer circle is the front circle (SVG path element) with stroke of the specified color in options,
     * which represents the current actual value of the slider.
     * Handler circle is placed at the current position of the outer circle offset
     * and has mousedown and touchstart event listeners bound to it.
     * Value (offset) of the outer circle (and handler position) is initialized
     * with a random value.
     */
    const CX = DRAW_WIDTH / 2;
    const CY = DRAW_HEIGHT / 2;
    const START_OFFSET = Math.random() * 360;
    const MIN_OFFSET = 0;
    const MAX_OFFSET = 359;

    const MIN_POS = polarToCartesian(MIN_OFFSET);
    const MAX_POS = polarToCartesian(MAX_OFFSET);

    const innerCircle = new SVGElement('circle', {
        'cx': CX,
        'cy': CY,
        'r': RADIUS,
        'fill': 'none',
        'stroke': 'lightgray',
        'stroke-width': 25 * SCALE,
        'stroke-dasharray': '8 1',
        'stroke-dashoffset': '0'
    });
    CONTAINER.appendChild(innerCircle.element());

    const startPos = polarToCartesian(START_OFFSET);
    const largeArcSweep = START_OFFSET <= 180 ? '0' : '1';
    var OUTER_CIRCLE = new SVGElement('path', {
        'fill': 'none',
        'stroke': COLOR,
        'stroke-width': 25 * SCALE,
        'd': ['M', CX, CY-RADIUS,
              'A', RADIUS, RADIUS, 0, largeArcSweep, 1, startPos['x'], startPos['y']].join(' ')
    });
    CONTAINER.appendChild(OUTER_CIRCLE.element());

    var HANDLER = new SVGElement('circle', {
        'cx': startPos['x'],
        'cy': startPos['y'],
        'r': 20 * SCALE,
        'fill': 'white',
        'stroke': 'gray',
        'stroke-width': 2
    });
    CONTAINER.appendChild(HANDLER.element());

    /**
     * Data calculations and update logic.
     * Calculate number of bins and actual bin borders.
     * Find the correct bin according to the current offset value and update the data (SVG text element).
     */
    const N_BINS = (MAX - MIN) / STEP + 1;
    var BINS = [];
    for (var i = 0; i < N_BINS; i++)
        BINS.push(i * STEP + MIN);
    function findBin(ratio) {
        const val = ratio * (MAX - MIN) + MIN;
        for (var i = 1; i <= N_BINS; i++) {
            if (val < BINS[i])
                return BINS[i - 1];
        }
    }
    function updateData(offset) {
        switch (offset) {
            case MIN_OFFSET:
                text.element().textContent = MIN;
                break;
            case MAX_OFFSET:
                text.element().textContent = MAX;
                break;
            default:
                text.element().textContent = findBin(offset / 360);
        }
    }

    /**
     * Draw data elements.
     * Data elements are placed in a SVG group with an id 'dataPlaceholder'.
     * When the first slider is created the placeholder is created and
     * appended to the container. In further iterations, just a reference to the
     * placeholder is obtained (it is not recreated).
     * Elements appended to the placeholder are SVG groups which consist of
     * small colored rectangles and a text element (used for displaying the bin).
     */
    var placeholder = CONTAINER.getElementById('dataPlaceholder');
    if (placeholder === null) {
        placeholder = new SVGElement('g', {
            'id': 'dataPlaceholder',
            'transform': 'translate(' + (PORTRAIT ? 0 : DRAW_WIDTH) + ' '
                                      + (PORTRAIT ? DRAW_HEIGHT : (0.1*DATA_HEIGHT)) + ')'
        }).element();
        CONTAINER.appendChild(placeholder);
    }

    const counter = placeholder.getElementsByTagName('g').length;
    var g = new SVGElement('g', {
        'transform': 'translate(' + (PORTRAIT ? (0.4*DATA_WIDTH) : (0.05*DATA_WIDTH)) + ' '
                                  + (PORTRAIT ? (counter * 0.15*DATA_HEIGHT) : (counter * 0.1*DATA_HEIGHT)) + ')'
    });
    placeholder.appendChild(g.element());

    var rect = new SVGElement('rect', {
        'width': PORTRAIT ? (0.05*DATA_WIDTH) : (0.05*DATA_HEIGHT),
        'height': PORTRAIT ? (0.05*DATA_WIDTH) : (0.05*DATA_HEIGHT),
        'fill': COLOR,
    });
    g.appendChild(rect.element());

    var text = new SVGElement('text', {
        'x': PORTRAIT ? (0.1*DATA_WIDTH) : (0.1*DATA_HEIGHT),
        'y': PORTRAIT ? (0.04*DATA_WIDTH) : (0.04*DATA_HEIGHT),
        'fill': 'black',
        'font-size': '16px',
        'font-family': 'Verdana'
    });
    g.appendChild(text.element());

    updateData(START_OFFSET);

    /**
     * Setup event listeners.
     * Handler circle element is bound with 'mousedown' and 'touchstart' event listeners.
     * They both prevent default behavior of the event (such as scrolling and text highlighting of other elements in the
     * container while moving a finger on a mobile device), as well as add 'mousemove'/'touchmove' event listeners to
     * the window element, so that if handler loses focus sliding actually continues.
     * 'touchmove' event listener ignores multi-touch events.
     * 'mouseup'/'touchend' event listeners remove previously added listeners.
     */
    HANDLER.addEventListener('mousedown', mousedown);
    HANDLER.addEventListener("touchstart", touchstart);
    function mousedown(event) {
        event.preventDefault();
        window.addEventListener('mousemove', mousemove);
        window.addEventListener("mouseup", mouseup);
    }
    function touchstart(event) {
        event.preventDefault();
        window.addEventListener('touchmove', touchmove);
        window.addEventListener("touchend", touchend);
    }
    function mousemove(event) {
        event.preventDefault();
        var x = event.clientX;
        var y = event.clientY;
        moveHandler(x, y);
    }
    function touchmove(event) {
        event.preventDefault();
        // Ignore multi-touch
        if (event.touches.length > 1)
            return;
        var x = event.touches[0].pageX;
        var y = event.touches[0].pageY;
        moveHandler(x, y);
    }
    function mouseup(event) {
        event.preventDefault();
        window.removeEventListener('mousemove', mousemove);
        window.removeEventListener('mouseup', mouseup);
    }
    function touchend(event) {
        event.preventDefault();
        window.removeEventListener('touchmove', touchmove);
        window.removeEventListener('touchend', touchend);
    }

    /**
     * Move handler logic.
     * 'moveHandler' function is called from mousemove/touchmove event listeners.
     * It updates the positon of the handler, value (offset) of the outer circle and updates the data.
     * Handler is locked in two different situations:
     *  - when sliding in counterclockwise direction and reaching minimum (top of the circle)
     *  - when sliding in clockwise direction and reaching maximum (top of the circle)
     */
    function setHandlerPosition(cx, cy) {
        HANDLER.set('cx', cx);
        HANDLER.set('cy', cy);
    }
    function setCircleOffset(x, y) {
        const largeArcSweep = x >= CX ? '0' : '1';
        OUTER_CIRCLE.set('d', ['M', CX, CY-RADIUS,
            'A', RADIUS, RADIUS, 0, largeArcSweep, 1, x, y].join(' '));
    }

    var oldX = CX;
    function moveHandler(x, y) {
        // calculate new handler position
        const dx = x - CX;
        const dy = y - CY;
        const scale = RADIUS / Math.hypot(dx, dy);
        var newX = dx * scale + CX;
        var newY = dy * scale + CY;
        var offset;

        // lock handler circle if it reaches min from the anticlockwise direction
        if (oldX > CX && newX <= CX && newY < CY) {
            newX = MIN_POS['x'];
            newY = MIN_POS['y'];
            offset = MIN_OFFSET;
        }
        // lock handler circle if it reaches max from the clockwise direction
        else if (oldX < CX && newX >= CX && newY < CY) {
            newX = MAX_POS['x'];
            newY = MAX_POS['y'];
            offset = MAX_OFFSET;
        }
        // otherwise
        else {
            // calculate offset (between 0 and 360 degrees)
            var fi = Math.atan(dx / -dy);
            if (y >= CY)
                fi += Math.PI;
            else if (x <= CX && y <= CY)
                fi += 2 * Math.PI;
            offset = radToDeg(fi);

            // save current x coordinate
            oldX = newX;
        }

        // set new handler circle position
        setHandlerPosition(newX, newY);

        // change outer circle offset
        setCircleOffset(newX, newY);

        // update data panel
        updateData(offset);
    }
}

/**
 * Helper class for creating and manipulating SVG elements.
 * @param type      SVG element type
 * @param options   an object of element attributes
 */
function SVGElement(type, options) {
    this.create = function (type) {
        this.elem = document.createElementNS("http://www.w3.org/2000/svg", type);
    };
    this.get = function(key) {
        return this.elem.getAttributeNS(null, key);
    };
    this.set = function (key, val) {
        this.elem.setAttributeNS(null, key, val);
    };
    this.setBulk = function (options) {
        for (var key in options) {
            if (options.hasOwnProperty(key))
                this.set(key, options[key]);
        }
    };
    this.addClass = function(cls) {
        this.elem.classList.add(cls);
    };
    this.removeClass = function(cls) {
        this.elem.classList.remove(cls);
    };
    this.addEventListener = function(type, func) {
        this.elem.addEventListener(type, func);
    };
    this.removeEventListener = function(type, func) {
        this.elem.removeEventListener(type, func);
    };
    this.appendChild = function (child) {
        this.elem.appendChild(child);
    };
    this.removeChild = function (child) {
        this.elem.removeChild(child);
    };
    this.element = function() {
        return this.elem;
    };

    this.create(type);
    if (options !== null)
        this.setBulk(options);
}